﻿namespace Player
{
    public interface ITicker
    {
        void Update();
    }
}
﻿using System;

namespace Player
{
    public interface IInputController : ITicker
    {
        event EventHandler Moved;
        event EventHandler Finishing;

        float HorizontalInput { get; }
        float VerticalInput { get; }
    }
}
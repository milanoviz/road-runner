﻿using System;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private Player.PlayerManager _player;
        [SerializeField] private Rigidbody[] _allRigidbodys;
        [SerializeField] private float _spawnDistance;
        
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
        }

        private void OnEnable()
        {
            _player.Attack += GetDamage;
            SwitchPhysical(true);
        }

        private void OnDisable()
        {
            _player.Attack -= GetDamage;
            Invoke(nameof(Respawn), 1);
        }

        private void SwitchPhysical(bool value)
        {
            _animator.enabled = value;
            
            foreach (var element in _allRigidbodys)
            {
                element.isKinematic = value;
            }
        }
        
        private void GetDamage(object sender, EventArgs e)
        {
            Invoke(nameof(Die), 0.5f);
        }

        private void Die()
        {
            SwitchPhysical(false);
            Invoke(nameof(Destroy), 5);
        }
        
        private void Destroy()
        {
            gameObject.SetActive(false);
        }

        private void Respawn()
        {
            var spawnPosition = Random.insideUnitSphere.normalized * _spawnDistance;
            transform.position = new Vector3(spawnPosition.x, transform.position.y, spawnPosition.z);
            
            gameObject.SetActive(true);
        }
    }
}
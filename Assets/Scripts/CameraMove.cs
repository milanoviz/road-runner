using System;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] private Transform _target;
   
    private Vector3 _offset;
    
    private Transform _transform;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void Start()
    {
        _offset = _transform.position - _target.transform.position;
    }

    private void LateUpdate()
    {
        _transform.position = _target.position + _offset;
    }
}

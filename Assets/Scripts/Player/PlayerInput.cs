﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerInput : IInputController
    {
        public event EventHandler Moved;
        
        public event EventHandler Finishing;
        public float HorizontalInput { get; private set; }
        public float VerticalInput { get; private set; }

        public void Update()
        {
            HorizontalInput = Input.GetAxis("Horizontal");
            VerticalInput = Input.GetAxis("Vertical");
            if (Math.Abs(HorizontalInput) > 0 || Math.Abs(VerticalInput) > 0)
            {
                OnMoved();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                OnFinishing();
            }
        }

        private void OnMoved()
        {
            Moved?.Invoke(this, EventArgs.Empty);
        }
        
        private void OnFinishing()
        {
            Finishing?.Invoke(this, EventArgs.Empty);
        }
    }
}
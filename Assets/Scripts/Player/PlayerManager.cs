﻿using System;
using System.Collections;
using DefaultNamespace;
using UnityEngine;

namespace Player
{
    public class PlayerManager : MonoBehaviour
    {
        public event EventHandler Attack;
        public event EventHandler<bool> DistanceToFinishing;
        
        [Header("Player Settings")]
        [SerializeField] private PlayerData _playerData;
        [Header("Weapons")]
        [SerializeField] private GameObject _gun;
        [SerializeField] private GameObject _sword;
        [Header("Transforms")] 
        [SerializeField] private Transform _enemy;
        [SerializeField] private Transform _body;
        [Header("Distances")] 
        [SerializeField] private float _maxFinishingDistance = 8.0f;
        [SerializeField] private float _minFinishingDistance = 1.65f;
        
        private IInputController _playerInput;
        
        private Vector3 _playerDirection;
        private Vector3 _moveDirection;
        private Vector3 _rotateDirection;
        
        private Camera _camera;
        private Animator _animator;
        private CharacterController _characterController;
        
        public void Init(IInputController inputController)
        {
            _playerInput = inputController;
            _playerInput.Moved += MovedHandler;
            _playerInput.Finishing += FinishingHandler;
        }
        
        private void Awake()
        {
            _camera = Camera.main;
            _animator = GetComponentInChildren<Animator>();
            _characterController = GetComponent<CharacterController>();
        }

        private void Start()
        {
            _gun.SetActive(true);
            _sword.SetActive(false);
        }

        private void Update()
        {
            CheckDistanceToEnemy();
            CheckCurrentAnimation();
        }

        private void MovedHandler(object sender, EventArgs eventArgs)
        {
            Movement();
            Rotated();
        }

        private void Movement()
        {
            var horizontalInput = _playerInput.HorizontalInput;
            var verticalInput = _playerInput.VerticalInput;
            
            _playerDirection = transform.TransformDirection(horizontalInput, 0, verticalInput);
            _playerDirection.y = -_playerData.Gravity;
            
            _moveDirection = _playerDirection * _playerData.SpeedMove * Time.deltaTime;
            _characterController.Move(_moveDirection);
            
            SetValueForAnimations(verticalInput, horizontalInput);
        }

        private void Rotated()
        {
            _playerDirection.y = 0;
            _rotateDirection = Vector3.RotateTowards(transform.forward, _playerDirection,
                _playerData.SpeedRotation * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(_rotateDirection);
        }
        
        private void FinishingHandler(object sender, EventArgs e)
        {
            StartCoroutine(nameof(Finishing));
        }

        private void SetValueForAnimations(float verticalInput, float horizontalInput)
        {
            _animator.SetFloat("Speed", verticalInput);
            _animator.SetFloat("Direction", horizontalInput);
        }
        
        private IEnumerator Finishing()
        {
            while (Vector3.Distance(transform.position, _enemy.position) > _minFinishingDistance)
            {
                _playerDirection = (_enemy.position - transform.position).normalized;
                _animator.SetFloat("Speed", 1);
                _characterController.Move(_playerDirection * _playerData.SpeedMove * Time.deltaTime);
                Rotated();
                yield return null;
            }
            _animator.SetFloat("Speed", 0);
            _animator.SetTrigger("Finishing");
            
            Attack?.Invoke(this, EventArgs.Empty);
        }
        
        private void CheckDistanceToEnemy()
        {
            OnDistanceToFinishing(Vector3.Distance(transform.position, _enemy.position) < _maxFinishingDistance);
        }

        private void CheckCurrentAnimation()
        {
            if (IsAnimationPlaying("Base Layer.Finishing"))
            {
                _sword.SetActive(true);
                _gun.SetActive(false);
            }
            else
            {
                _sword.SetActive(false);
                _gun.SetActive(true);
            }
        }
        
        private bool IsAnimationPlaying(string animationName)
        {
            var animatorStateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            return animatorStateInfo.IsName(animationName);
        }

        private void OnDistanceToFinishing(bool value)
        {
            DistanceToFinishing?.Invoke(this, value);
        }
    }
}
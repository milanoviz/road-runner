﻿using System;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private Text _textFinishing;
        
        private IInputController _inputController;

        private void Awake()
        {
            _inputController = new PlayerInput();
            _textFinishing.gameObject.SetActive(false);
            playerManager.Init(_inputController);
        }

        private void OnEnable()
        {
            playerManager.DistanceToFinishing += TextDistanceToFinishingHandler;
        }

        private void OnDisable()
        {
            playerManager.DistanceToFinishing -= TextDistanceToFinishingHandler;
        }

        private void TextDistanceToFinishingHandler(object sender, bool value)
        {
            _textFinishing.gameObject.SetActive(value);
        }

        private void Update()
        {
            _inputController.Update();
        }
    }
}
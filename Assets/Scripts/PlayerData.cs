﻿using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Player/Settings", fileName = "PlayerData")]
    public class PlayerData : ScriptableObject
    {
        [Header("Movement")]
        [SerializeField] private float _speedMove;
        [SerializeField] private float _speedRotation;
        [SerializeField] private float _gravity;

        public float SpeedMove => _speedMove;
        public float SpeedRotation => _speedRotation;
        public float Gravity => _gravity;
    }
}